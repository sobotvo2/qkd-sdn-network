package cz.cvut.fel.qkd.kms.exception;

public class NoKeyFoundException extends RuntimeException {
  public NoKeyFoundException(String id) {
    super("Key with id " + id + " not found");
  }
}
