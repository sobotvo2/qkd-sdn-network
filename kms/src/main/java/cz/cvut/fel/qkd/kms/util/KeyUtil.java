package cz.cvut.fel.qkd.kms.util;

import java.security.SecureRandom;
import lombok.experimental.UtilityClass;

@UtilityClass
public class KeyUtil {
  public static final int DEFAULT_KEY_SIZE = 256;

    public static String bitStringToString(String key) {
      char[] chars = new char[32];
      for (int i = 0; i < 32; i++) {
        //Slice the string of bits to bytes
        String curSeq = key.substring(i * 8, i * 8 + 8);
        //Transfer bits to int
        int codePoint = Integer.parseInt(curSeq, 2);
        //Get a char from int
        String utf8Char = new String(Character.toChars(codePoint));
        chars[i] = utf8Char.charAt(0);
      }
      return new String(chars);
    }

    public static String stringToBitString(String key) {
      StringBuilder builder = new StringBuilder();
      for (int i = 0; i < 32; i++) {
        //Read the char at a certain position
        String binaryString = Integer.toBinaryString(key.charAt(i));
        //Transfer char to binary
        binaryString = String.format("%8s", binaryString).replace(' ', '0');
        //Append to string
        builder.append(binaryString);
      }
      return builder.toString();
    }

  public static String generateRandom256BitString() {
    SecureRandom secureRandom = new SecureRandom();
    char[] chars = new char[256];
    for (int i = 0; i < 256; i++) {
      chars[i] = secureRandom.nextBoolean() ? '1' : '0';
    }
    return new String(chars);
  }

  public static String generateRandomString(int defaultKeySize) {
    return bitStringToString(generateRandom256BitString());
  }

  public static String xor(String applicationKey, String decryptionKey) {
    applicationKey = stringToBitString(applicationKey);
    decryptionKey = stringToBitString(decryptionKey);

    StringBuilder result = new StringBuilder();

    // Ensure both binary strings have the same length
    int length = Math.min(applicationKey.length(), decryptionKey.length());
    for (int i = 0; i < length; i++) {
      // Perform XOR operation on each pair of corresponding bits
      char bit1 = applicationKey.charAt(i);
      char bit2 = decryptionKey.charAt(i);
      char xorResult = (bit1 != bit2) ? '1' : '0';
      result.append(xorResult);
    }

    return bitStringToString(result.toString());
  }
}
