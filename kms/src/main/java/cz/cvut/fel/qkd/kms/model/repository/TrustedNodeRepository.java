package cz.cvut.fel.qkd.kms.model.repository;

import cz.cvut.fel.qkd.kms.model.entity.TrustedNode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrustedNodeRepository extends JpaRepository<TrustedNode, String> {}
