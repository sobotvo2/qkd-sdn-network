package cz.cvut.fel.qkd.kms.service;

import cz.cvut.fel.qkd.kms.async.publisher.KeyEventPublisher;
import cz.cvut.fel.qkd.kms.exception.NoKeyFoundException;
import cz.cvut.fel.qkd.kms.model.entity.ApplicationKey;
import cz.cvut.fel.qkd.kms.model.entity.EncryptionKey;
import cz.cvut.fel.qkd.kms.model.entity.KeyRouting;
import cz.cvut.fel.qkd.kms.model.repository.ApplicationKeyRepository;
import cz.cvut.fel.qkd.kms.model.repository.KeyRoutingRepository;
import cz.cvut.fel.qkd.kms.util.KeyUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log
public class QkdModuleService {
  private final KeyEventPublisher keyEventPublisher;
  private final ApplicationKeyRepository applicationKeyRepository;
  private final MockQKDModule qkdModule;
  private final KeyRoutingRepository keyRoutingRepository;

  public void decryptKey(String keyId) {
    log.info("decryptKey: " + keyId);

    ApplicationKey applicationKey =
        applicationKeyRepository
            .findById(keyId)
            .orElseThrow(() -> new IllegalArgumentException("Invalid keyId: " + keyId));

    KeyRouting keyRouting = applicationKey.getKeyRouting();

    String rawKey = applicationKey.getApplicationKey();
    String decryptionKey =
        qkdModule.genEncryptionKeyIdById(
            applicationKey.getDecryptionKeyId(), keyRouting.getPreviousNodeId().getName());

    String decryptedApplicationKey = KeyUtil.xor(rawKey, decryptionKey);
    applicationKey.setApplicationKey(decryptedApplicationKey);
    applicationKey.setDecryptedAfterReceiving(true);

    applicationKeyRepository.save(applicationKey);
    log.info("decryptedKey: " + decryptionKey);

    if (keyRouting.getNextNodeId() != null) {
      keyEventPublisher.publishEncryptKeyEvent(keyId);
    } else {
      log.info("Key with id " + applicationKey.getId() + " arrived to final destination!");
    }
  }

  public void encryptKey(String keyId) {
    log.info("encryptKey: " + keyId);
    ApplicationKey applicationKey =
        applicationKeyRepository.findById(keyId).orElseThrow(() -> new NoKeyFoundException(keyId));

    KeyRouting keyRouting = applicationKey.getKeyRouting();

    EncryptionKey encryptionKey =
        qkdModule.getEncryptionKeyForRelayString(keyRouting.getNextNodeId().getName());

    String encryptedApplicationKey =
        KeyUtil.xor(applicationKey.getApplicationKey(), encryptionKey.getEncryptionKey());

    applicationKey.setApplicationKey(encryptedApplicationKey);
    applicationKey.setEncryptionKeyId(encryptionKey.getKeyId());
    applicationKey.setEncryptedBeforeSending(true);

    applicationKeyRepository.save(applicationKey);

    log.info("encryptedKey: " + keyId);
    keyEventPublisher.publishForwardKeyEvent(keyId);
  }
}
