package cz.cvut.fel.qkd.kms.async.interceptor;

import cz.cvut.fel.qkd.kms.async.event.EncryptKeyEvent;
import cz.cvut.fel.qkd.kms.service.QkdModuleService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class EncryptionKeyEventInterceptor implements ApplicationListener<EncryptKeyEvent> {
  private final QkdModuleService moduleService;

  @Override
  public void onApplicationEvent(EncryptKeyEvent event) {
    moduleService.encryptKey(event.getKeyId());
  }
}
