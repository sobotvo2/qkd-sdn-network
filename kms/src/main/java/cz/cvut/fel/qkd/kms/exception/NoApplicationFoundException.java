package cz.cvut.fel.qkd.kms.exception;

public class NoApplicationFoundException extends RuntimeException {
  public NoApplicationFoundException(String appId) {
    super("No application found with id " + appId);
  }
}
