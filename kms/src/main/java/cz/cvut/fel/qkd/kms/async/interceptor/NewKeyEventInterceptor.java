package cz.cvut.fel.qkd.kms.async.interceptor;

import cz.cvut.fel.qkd.kms.async.event.NewKeyEvent;
import cz.cvut.fel.qkd.kms.service.QkdModuleService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NewKeyEventInterceptor implements ApplicationListener<NewKeyEvent> {
  private final QkdModuleService moduleService;

  @Override
  public void onApplicationEvent(NewKeyEvent event) {
    moduleService.decryptKey(event.getKeyId());
  }
}
