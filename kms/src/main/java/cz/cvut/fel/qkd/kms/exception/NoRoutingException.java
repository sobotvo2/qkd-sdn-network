package cz.cvut.fel.qkd.kms.exception;

public class NoRoutingException extends RuntimeException {
  public NoRoutingException(String startNodeId, String endNodeId) {
    super("No routing for " + startNodeId + " and " + endNodeId);
  }
}
