package cz.cvut.fel.qkd.kms.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
public class EncryptionKey {
  @Id private String keyId;
  @Column private String encryptionKey;
  @Column private String fromNodeName;
  @Column private String toNodeName;
  @Column private String used;
  @Column private LocalDateTime usedAt;

  public EncryptionKey(String keyId, String key, String fromNodeName, String toNodeName) {
    this.keyId = keyId;
    this.encryptionKey = key;
    this.fromNodeName = fromNodeName;
    this.toNodeName = toNodeName;
  }
}
