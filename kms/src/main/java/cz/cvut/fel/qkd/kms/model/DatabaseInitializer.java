package cz.cvut.fel.qkd.kms.model;

import cz.cvut.fel.qkd.kms.model.entity.QkdSecuredApplication;
import cz.cvut.fel.qkd.kms.model.entity.TrustedNode;
import cz.cvut.fel.qkd.kms.model.repository.QkdSecuredApplicationRepository;
import cz.cvut.fel.qkd.kms.model.repository.TrustedNodeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Log
public class DatabaseInitializer implements ApplicationRunner {
    private final String ALPHA = "ALPHA";
    private final String BETA = "BETA";
    private final String GAMMA = "GAMMA";
    private final String ALPHA_ADDRESSES = "http://localhost:8081";
    private final String BETA_ADDRESSES = "http://localhost:8082";
    private final String GAMMA_ADDRESSES = "http://localhost:8083";
    private final String MASTER = "MASTER";
    private final String SLAVE = "SLAVE";
    private final TrustedNodeRepository trustedNodeRepository;
    private final QkdSecuredApplicationRepository qkdSecuredApplicationRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<TrustedNode> trustedNodes =
                trustedNodeRepository.saveAll(
                        List.of(
                                TrustedNode.builder().name(ALPHA).address(ALPHA_ADDRESSES).build(),
                                TrustedNode.builder().name(BETA).address(BETA_ADDRESSES).build(),
                                TrustedNode.builder().name(GAMMA).address(GAMMA_ADDRESSES).build()));

        TrustedNode alpha = trustedNodes.get(0);
        TrustedNode beta = trustedNodes.get(1);
        TrustedNode gamma = trustedNodes.get(2);
        alpha.setConnectedNodes(List.of(beta));
        beta.setConnectedNodes(List.of(gamma, alpha));
        gamma.setConnectedNodes(List.of(beta));
        trustedNodeRepository.save(alpha);
        trustedNodeRepository.save(beta);
        trustedNodeRepository.save(gamma);
        qkdSecuredApplicationRepository.saveAll(
                List.of(
                        QkdSecuredApplication.builder().name(MASTER).build(),
                        QkdSecuredApplication.builder().name(SLAVE).build()));

        log.info("Database was initialized");
    }
}
