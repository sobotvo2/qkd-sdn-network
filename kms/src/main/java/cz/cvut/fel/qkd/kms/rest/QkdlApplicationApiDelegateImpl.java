package cz.cvut.fel.qkd.kms.rest;

import cz.cvut.fel.qkd.kms.openapi.server.api.QkdlApplicationApiDelegate;
import cz.cvut.fel.qkd.kms.openapi.server.model.SetupApplicationRequest;
import cz.cvut.fel.qkd.kms.service.RoutingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class QkdlApplicationApiDelegateImpl implements QkdlApplicationApiDelegate {
  private final RoutingService routingService;
  @Override
  public ResponseEntity<Void> setupApplication(SetupApplicationRequest setupApplicationRequest) {
    routingService.addNewQkdApplication(setupApplicationRequest);
    return ResponseEntity.ok().build();
  }
}
