package cz.cvut.fel.qkd.kms.service;

import cz.cvut.fel.qkd.kms.model.entity.EncryptionKey;
import org.springframework.stereotype.Service;

@Service
public interface EncryptionKeyService {
  /*
   * Get new encryption key from QKD module
   */
  EncryptionKey getEncryptionKeyForRelayString(String toNode);

  /*
   * Get decryption key by Id and related node
   */
  String genEncryptionKeyIdById(String id, String fromNode);
}
