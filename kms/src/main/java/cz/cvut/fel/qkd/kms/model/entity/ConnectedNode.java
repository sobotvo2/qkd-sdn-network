package cz.cvut.fel.qkd.kms.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
/*
 Support table to hold information for QKD modules
 */
public class ConnectedNode {
  @Id @GeneratedValue private Long id;
  @Column private String sourceNode;
  @Column private String destinationNode;
  @Column private String qkdModuleAddress;
  @Column private String qkdModuleId;
}
