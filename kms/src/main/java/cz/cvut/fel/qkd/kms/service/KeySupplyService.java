package cz.cvut.fel.qkd.kms.service;

import cz.cvut.fel.qkd.kms.async.publisher.KeyEventPublisher;
import cz.cvut.fel.qkd.kms.exception.NoApplicationFoundException;
import cz.cvut.fel.qkd.kms.exception.NoRoutingException;
import cz.cvut.fel.qkd.kms.model.entity.ApplicationKey;
import cz.cvut.fel.qkd.kms.model.entity.KeyRouting;
import cz.cvut.fel.qkd.kms.model.entity.QkdSecuredApplication;
import cz.cvut.fel.qkd.kms.model.repository.ApplicationKeyRepository;
import cz.cvut.fel.qkd.kms.model.repository.KeyRoutingRepository;
import cz.cvut.fel.qkd.kms.model.repository.QkdSecuredApplicationRepository;
import cz.cvut.fel.qkd.kms.openapi.fetch.server.model.Key;
import cz.cvut.fel.qkd.kms.openapi.fetch.server.model.KeyContainer;
import cz.cvut.fel.qkd.kms.util.KeyUtil;
import java.time.LocalDateTime;
import java.util.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log
public class KeySupplyService {
  private final ApplicationKeyRepository applicationKeyRepository;
  private final KeyRoutingRepository keyRoutingRepository;
  private final KeyEventPublisher keyEventPublisher;
  private final QkdSecuredApplicationRepository qkdSecuredApplicationRepository;

  public KeyContainer getKeyToSlave(String masterSAEID, String slaveSAEID, String keyID) {
    ApplicationKey applicationKey =
        applicationKeyRepository
            .findById(keyID)
            .orElseThrow(() -> new NoApplicationFoundException(keyID));

    if (!Objects.equals(masterSAEID, applicationKey.getKeyRouting().getMasterSae().getName())
        && !Objects.equals(slaveSAEID, applicationKey.getKeyRouting().getSlaveSae().getName())) {
      throw new NoRoutingException(masterSAEID, slaveSAEID);
    }
    return new KeyContainer(
        List.of(new Key(applicationKey.getId(), applicationKey.getApplicationKey())));
  }

  public KeyContainer getKeysToMaster(
      String masterSAEID, String slaveSAEID, Integer number, Integer size) {
    QkdSecuredApplication masterApp =
        qkdSecuredApplicationRepository
            .findById(masterSAEID)
            .orElseThrow(() -> new NoApplicationFoundException(masterSAEID));
    QkdSecuredApplication slaveApp =
        qkdSecuredApplicationRepository
            .findById(masterSAEID)
            .orElseThrow(() -> new NoApplicationFoundException(slaveSAEID));
    KeyRouting keyRouting =
        keyRoutingRepository.findAll().stream()
            .filter(
                x ->
                    Objects.equals(x.getMasterSae().getName(), masterSAEID)
                        && Objects.equals(x.getSlaveSae().getName(), slaveSAEID))
            .max(Comparator.comparingInt(a -> Integer.parseInt(a.getAppId())))
            .orElseThrow(() -> new NoRoutingException(masterSAEID, slaveSAEID));

    List<Key> keys = new ArrayList<>();
    for (int i = 0; i < number; i++) {
      ApplicationKey applicationKey =
          ApplicationKey.builder()
              .id(UUID.randomUUID().toString())
              .applicationKey(KeyUtil.generateRandomString(256))
              .decryptedAfterReceiving(true)
              .keyRouting(keyRouting)
              .createdAt(LocalDateTime.now())
              .build();

      applicationKeyRepository.save(applicationKey);
      log.info("Created application key " + applicationKey.getId());

      keys.add(new Key(applicationKey.getId(), applicationKey.getApplicationKey()));

      keyEventPublisher.publishEncryptKeyEvent(applicationKey.getId());
    }
    return new KeyContainer(keys);
  }
}
