package cz.cvut.fel.qkd.kms.service;

import cz.cvut.fel.qkd.kms.model.entity.ApplicationKey;
import cz.cvut.fel.qkd.kms.model.repository.ApplicationKeyRepository;
import java.time.LocalDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Log
public class KeyDeletionService {

  private static final String NULL_KEY = "0000000000000000000000000000";
  private final ApplicationKeyRepository applicationKeyRepository;

  // Header with frequency, in which is the method invoked (2 minutes)
  @Scheduled(fixedRate = 120000)
  public void deleteOldKeys() {
    log.info("Running key deletion procedure");
    // Fetch of keys to enable the filterer of keys with functional interface used to commit
    // operation with exact dates in SQL(2 minutes)
    List<ApplicationKey> keys = applicationKeyRepository.findAll();
    // Functional finding of keys older than 10 minutes.
    keys =
        keys.stream()
            .filter(key -> key.getCreatedAt().isBefore(LocalDateTime.now().minusMinutes(10)))
            .toList();

    for (ApplicationKey key : keys) {
      log.info("Deleting key " + key.getId() + " of session " + key.getKeyRouting().getAppId() + " received at " + key.getCreatedAt());
      //OVERWRITING OF THE KEY
      key.setApplicationKey(NULL_KEY);
      applicationKeyRepository.save(key);
      //DELETION AND DEREFERENCE OF THE KEY
      applicationKeyRepository.deleteById(key.getId());
      //FORCING OF THE OPERATION
      applicationKeyRepository.flush();
    }
    //CLEAN OF THE JAVA MEMORY
    System.gc();
  }
}
