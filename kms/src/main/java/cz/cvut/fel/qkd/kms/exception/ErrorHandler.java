package cz.cvut.fel.qkd.kms.exception;

import static org.springframework.http.ResponseEntity.status;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandler {
  @ResponseBody
  @ExceptionHandler(CouldNotForwardKeyException.class)
  public ResponseEntity<ExceptionDto> handleCouldNotForwardKeyException(
      CouldNotForwardKeyException e) {
    return status(HttpStatus.INTERNAL_SERVER_ERROR)
        .contentType(MediaType.APPLICATION_JSON)
        .body(new ExceptionDto(CouldNotForwardKeyException.class.getName(), e.getMessage()));
  }

  @ResponseBody
  @ExceptionHandler(NoApplicationFoundException.class)
  public ResponseEntity<ExceptionDto> handleNoApplicationNotFoundException(
      NoApplicationFoundException e) {
    return status(HttpStatus.NOT_FOUND)
        .contentType(MediaType.APPLICATION_JSON)
        .body(new ExceptionDto(NoApplicationFoundException.class.getName(), e.getMessage()));
  }

  @ResponseBody
  @ExceptionHandler(NoKeyFoundException.class)
  public ResponseEntity<ExceptionDto> handleNoKeyFoundException(NoKeyFoundException e) {
    return status(HttpStatus.NOT_FOUND)
        .contentType(MediaType.APPLICATION_JSON)
        .body(new ExceptionDto(NoKeyFoundException.class.getName(), e.getMessage()));
  }

  @ResponseBody
  @ExceptionHandler(NoRoutingException.class)
  public ResponseEntity<ExceptionDto> handleNoRoutingException(NoRoutingException e) {
    return status(HttpStatus.NOT_FOUND)
        .contentType(MediaType.APPLICATION_JSON)
        .body(new ExceptionDto(NoRoutingException.class.getName(), e.getMessage()));
  }
}
