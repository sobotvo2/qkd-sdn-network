package cz.cvut.fel.qkd.kms.async.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class NewKeyEvent extends ApplicationEvent {
  private final String keyId;

  public NewKeyEvent(Object source, String keyId) {
    super(source);
    this.keyId = keyId;
  }
}
