package cz.cvut.fel.qkd.kms.service;

import cz.cvut.fel.qkd.kms.client.QkdModuleClient;
import cz.cvut.fel.qkd.kms.exception.NoKeyFoundException;
import cz.cvut.fel.qkd.kms.model.entity.EncryptionKey;
import cz.cvut.fel.qkd.kms.openapi.fetch.client.model.Key;
import cz.cvut.fel.qkd.kms.openapi.fetch.client.model.KeyContainer;
import java.util.Collections;
import java.util.Optional;
import lombok.RequiredArgsConstructor;

// @Service Uncomment when connected to the QKD modules
@RequiredArgsConstructor
public class RealQkdModuleService implements EncryptionKeyService {
  private final QkdModuleClient qkdModuleClient;

  /*This method is by client calling the
  GET /api/v1/keys/{slave_SAE_ID}/dec_keys
  to fetch a new key
  */
  @Override
  public EncryptionKey getEncryptionKeyForRelayString(String toNode) {

    KeyContainer keyContainer = qkdModuleClient.getOneEcryptionKey(toNode);
    Key key =
        Optional.ofNullable(keyContainer.getKeys()).orElse(Collections.emptyList()).stream()
            .findFirst()
            .orElseThrow(() -> new IllegalStateException("Module return no encryption key"));
    EncryptionKey encryptionKey = new EncryptionKey();
    encryptionKey.setKeyId(key.getKeyID());
    encryptionKey.setEncryptionKey(key.getKey());
    return encryptionKey;
  }

  /*
  This method is by client calling the
   GET /api/v1/keys/{master_SAE_ID}/dec_keys
  to get a decryption key by ID.
   */
  @Override
  public String genEncryptionKeyIdById(String id, String fromNode) {
    String returnedKey = qkdModuleClient.getOneEncryptionKeyByIdAndRelatedAdress(id, fromNode);
    if (returnedKey == null || returnedKey.isEmpty()) {
      throw new NoKeyFoundException(id);
    } else {
      return returnedKey;
    }
  }
}
