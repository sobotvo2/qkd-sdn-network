package cz.cvut.fel.qkd.kms.client;

import cz.cvut.fel.qkd.kms.exception.CouldNotForwardKeyException;
import cz.cvut.fel.qkd.kms.exception.NoRoutingException;
import cz.cvut.fel.qkd.kms.model.entity.KeyRouting;
import cz.cvut.fel.qkd.kms.model.repository.KeyRoutingRepository;
import cz.cvut.fel.qkd.kms.model.repository.TrustedNodeRepository;
import cz.cvut.fel.qkd.kms.openapi.key.client.ApiClient;
import cz.cvut.fel.qkd.kms.openapi.key.client.api.KeyRelayApi;
import cz.cvut.fel.qkd.kms.openapi.key.client.model.KeyRelayRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

@Service
@RequiredArgsConstructor
@Log
public class KeyRelayClient {
  private final TrustedNodeRepository trustedNodeRepository;

  public void keyRelay(KeyRelayRequest keyRelayRequest, String nodeName) {
    String address =
        trustedNodeRepository
            .findById(nodeName)
            .orElseThrow(() -> new NoRoutingException("Node not found", nodeName))
            .getAddress();
    relayKeyToAnotherKms(address, keyRelayRequest);
  }

  private void relayKeyToAnotherKms(String address, KeyRelayRequest keyRelayRequest) {
    ApiClient apiClient = new ApiClient();
    apiClient.setBasePath(address);
    KeyRelayApi keyRelayApi = new KeyRelayApi(apiClient);

    log.info("Sending key relay request: " + keyRelayRequest + " to " + address);
    try {
      keyRelayApi.keyRelay(keyRelayRequest);
    } catch (RestClientException e) {
      log.info(e.getMessage());
      throw new CouldNotForwardKeyException(address, keyRelayRequest.getKeyId());
    }
  }
}
