package cz.cvut.fel.qkd.kms.model.repository;

import cz.cvut.fel.qkd.kms.model.entity.ConnectedNode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectedNodeRepository extends JpaRepository<ConnectedNode, Long> {

}
