package cz.cvut.fel.qkd.kms.rest;

import cz.cvut.fel.qkd.kms.openapi.key.server.api.KeyRelayApiDelegate;
import cz.cvut.fel.qkd.kms.openapi.key.server.model.KeyRelayRequest;
import cz.cvut.fel.qkd.kms.service.KeyRelayService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KeyRelayDelegateImpl implements KeyRelayApiDelegate {
  private final KeyRelayService keyRelayService;

  @Override
  public ResponseEntity<Void> keyRelay(KeyRelayRequest keyRelayRequest) {
    keyRelayService.receiveKey(keyRelayRequest);
    return ResponseEntity.ok().build();
  }
}
