package cz.cvut.fel.qkd.kms.model.repository;

import cz.cvut.fel.qkd.kms.model.entity.KeyRouting;
import cz.cvut.fel.qkd.kms.model.entity.QkdSecuredApplication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KeyRoutingRepository extends JpaRepository<KeyRouting, String> {
  List<KeyRouting> findAllByMasterSaeAndSlaveSae(QkdSecuredApplication masterSae, QkdSecuredApplication slaveSae);
}
