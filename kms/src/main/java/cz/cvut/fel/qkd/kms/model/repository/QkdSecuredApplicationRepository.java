package cz.cvut.fel.qkd.kms.model.repository;

import cz.cvut.fel.qkd.kms.model.entity.QkdSecuredApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QkdSecuredApplicationRepository
    extends JpaRepository<QkdSecuredApplication, String> {}
