package cz.cvut.fel.qkd.kms.async.interceptor;

import cz.cvut.fel.qkd.kms.async.event.ForwardKeyEvent;
import cz.cvut.fel.qkd.kms.service.KeyRelayService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ForwardKeyInterceptor implements ApplicationListener<ForwardKeyEvent> {
  private final KeyRelayService keyRelayService;

  @Override
  public void onApplicationEvent(ForwardKeyEvent event) {
    keyRelayService.forwardKey(event.getKeyId());
  }
}
