package cz.cvut.fel.qkd.kms.model.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@RequiredArgsConstructor
@Entity
@Builder
@AllArgsConstructor
public class TrustedNode {
  @Id
  private  String name;
  private  String address;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
          name = "connected_node2",
          joinColumns = @JoinColumn(name = "start_name"),
          inverseJoinColumns = @JoinColumn(name = "end_name")
  )
  private List<TrustedNode> connectedNodes;

  @ManyToMany(mappedBy = "connectedNodes")
  private List<TrustedNode> isConnectedToNodes;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  List<KeyRouting> isPreviousNodeInRouting;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  List<KeyRouting> isNextNodeInRouting;
}