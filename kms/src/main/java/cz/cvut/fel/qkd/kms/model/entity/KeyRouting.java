package cz.cvut.fel.qkd.kms.model.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Data
@ToString(doNotUseGetters = true)
@NoArgsConstructor
@Builder
@AllArgsConstructor()
public class KeyRouting {
  @Id private String appId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn()
  private TrustedNode previousNodeId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn()
  private TrustedNode nextNodeId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn()
  private QkdSecuredApplication masterSae;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn()
  private QkdSecuredApplication slaveSae;

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private List<ApplicationKey> applicationKeys;
}
