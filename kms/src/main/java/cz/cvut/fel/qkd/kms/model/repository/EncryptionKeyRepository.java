package cz.cvut.fel.qkd.kms.model.repository;

import cz.cvut.fel.qkd.kms.exception.NoKeyFoundException;
import cz.cvut.fel.qkd.kms.model.entity.EncryptionKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import lombok.Getter;
import org.springframework.stereotype.Repository;


/*
  This class is used as simulation of mocked QKD modules
 */
@Repository
public class EncryptionKeyRepository {
  @Getter private final String ALPHA_BETA_ENCRYPTION_KEY = "aFdLcBdaOrEbmhKpyanzeJgVKtsqoFjW";
  @Getter private final String BETA_ALPHA_ENCRYPTION_KEY = "qMsYpZaJrGkNbIvAcToXlDwFhCcVubLn";
  @Getter private final String BETA_GAMMA_ENCRYPTION_KEY = "aBcDeFgHiJkLmNoPrSqTuVwXyZaaadaa";
  @Getter private final String GAMMA_BETA_ENCRYPTION_KEY = "zeJgVKtssdsrEbmjKpyaFdLcBdaOrEbm";

  private final String ALPHA_BETA_ENCRYPTION_KEY_ID = "ALPHA_BETA_ENCRYPTION_KEY_ID";
  private final String BETA_ALPHA_ENCRYPTION_KEY_ID = "BETA_ALPHA_ENCRYPTION_KEY_ID";
  private final String BETA_GAMMA_ENCRYPTION_KEY_ID = "BETA_GAMMA_ENCRYPTION_KEY_ID";
  private final String GAMMA_BETA_ENCRYPTION_KEY_ID = "GAMMA_BETA_ENCRYPTION_KEY_ID";

  private final String ALPHA = "ALPHA";
  private final String BETA = "BETA";
  private final String GAMMA = "GAMMA";

  Map<String, EncryptionKey> encryptionKeys;

  public EncryptionKeyRepository() {
    encryptionKeys = new HashMap<>();
    encryptionKeys.put(
        ALPHA_BETA_ENCRYPTION_KEY_ID,
        new EncryptionKey(ALPHA_BETA_ENCRYPTION_KEY_ID, ALPHA_BETA_ENCRYPTION_KEY, ALPHA, BETA));
    encryptionKeys.put(
        BETA_ALPHA_ENCRYPTION_KEY_ID,
        new EncryptionKey(BETA_ALPHA_ENCRYPTION_KEY_ID, BETA_ALPHA_ENCRYPTION_KEY, BETA, ALPHA));
    encryptionKeys.put(
        BETA_GAMMA_ENCRYPTION_KEY_ID,
        new EncryptionKey(BETA_GAMMA_ENCRYPTION_KEY_ID, BETA_GAMMA_ENCRYPTION_KEY, BETA, GAMMA));
    encryptionKeys.put(
        GAMMA_BETA_ENCRYPTION_KEY_ID,
        new EncryptionKey(GAMMA_BETA_ENCRYPTION_KEY_ID, GAMMA_BETA_ENCRYPTION_KEY, GAMMA, BETA));
  }

  public EncryptionKey getNewEncryptionKeyById(final String id) {
    return Optional.ofNullable(encryptionKeys.get(id))
        .orElseThrow(() -> new NoKeyFoundException(id));
  }

  public EncryptionKey getNewEncryptionKeyById(
      final String nodeNameStart, final String nodeNameEnd) {
    return encryptionKeys.values().stream()
        .filter(x -> (Objects.equals(x.getFromNodeName(), nodeNameStart) && Objects.equals(x.getToNodeName(), nodeNameEnd)))
        .findFirst()
        .orElseThrow(() -> new NoKeyFoundException(nodeNameStart + " " + nodeNameEnd));
  }
}
