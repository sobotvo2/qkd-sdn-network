package cz.cvut.fel.qkd.kms.client;

import cz.cvut.fel.qkd.kms.exception.NoKeyFoundException;
import cz.cvut.fel.qkd.kms.openapi.fetch.client.ApiClient;
import cz.cvut.fel.qkd.kms.openapi.fetch.client.api.DefaultApi;
import cz.cvut.fel.qkd.kms.openapi.fetch.client.model.KeyContainer;
import cz.cvut.fel.qkd.kms.util.KeyUtil;
import java.util.Collections;
import java.util.Optional;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Log
public class QkdModuleClient {
  @Value("${node.name}")
  private String nodeName;

  public KeyContainer getOneEcryptionKey(String address) {
    ApiClient apiClient = new ApiClient();
    apiClient.setBasePath(address);
    DefaultApi keyApi = new DefaultApi(apiClient);
    return keyApi.apiV1KeysSlaveSAEIDEncKeysGet(nodeName, 1, KeyUtil.DEFAULT_KEY_SIZE);
  }

  public String getOneEncryptionKeyByIdAndRelatedAdress(String address, String id) {
    ApiClient apiClient = new ApiClient();
    apiClient.setBasePath(address);
    DefaultApi keyApi = new DefaultApi(apiClient);
    return Optional.ofNullable(keyApi.apiV1KeysMasterSAEIDDecKeysGet(nodeName, id).getKeys())
        .orElse(Collections.emptyList())
        .stream()
        .findFirst()
        .orElseThrow(() -> new NoKeyFoundException(id + " " + nodeName))
        .getKey();
  }
}
