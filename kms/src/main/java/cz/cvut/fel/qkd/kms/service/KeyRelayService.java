package cz.cvut.fel.qkd.kms.service;

import cz.cvut.fel.qkd.kms.async.publisher.KeyEventPublisher;
import cz.cvut.fel.qkd.kms.client.KeyRelayClient;
import cz.cvut.fel.qkd.kms.exception.NoKeyFoundException;
import cz.cvut.fel.qkd.kms.exception.NoRoutingException;
import cz.cvut.fel.qkd.kms.model.entity.ApplicationKey;
import cz.cvut.fel.qkd.kms.model.entity.KeyRouting;
import cz.cvut.fel.qkd.kms.model.repository.ApplicationKeyRepository;
import cz.cvut.fel.qkd.kms.model.repository.KeyRoutingRepository;
import cz.cvut.fel.qkd.kms.model.repository.TrustedNodeRepository;
import cz.cvut.fel.qkd.kms.openapi.key.client.model.KeyRelayRequest;
import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Log
public class KeyRelayService {
  private final KeyEventPublisher keyEventPublisher;
  private final ApplicationKeyRepository applicationKeyRepository;
  private final KeyRoutingRepository keyRoutingRepository;
    private final KeyRelayClient keyRelayClient;

  public void receiveKey(
      cz.cvut.fel.qkd.kms.openapi.key.server.model.KeyRelayRequest keyRelayRequest) {
    log.info("Received key relay request: " + keyRelayRequest);
    KeyRouting keyRouting =
        keyRoutingRepository
            .findById(keyRelayRequest.getAppId())
            .orElseThrow(() -> new NoRoutingException(keyRelayRequest.getAppId(), ""));

    ApplicationKey applicationKey =
        applicationKeyRepository.save(
            ApplicationKey.builder()
                .id(keyRelayRequest.getKeyId())
                .applicationKey(keyRelayRequest.getEncryptedKey())
                .keyRouting(keyRouting)
                .decryptionKeyId(keyRelayRequest.getEncriptionKeyId())
                .createdAt(LocalDateTime.now())
                .build());

    keyEventPublisher.publishNewKeyEvent(applicationKey.getId());
  }

  public void forwardKey(String keyId) {
    log.info("Forwarding key: " + keyId);
    ApplicationKey applicationKey =
        applicationKeyRepository.findById(keyId).orElseThrow(() -> new NoKeyFoundException(keyId));

    KeyRouting keyRouting = applicationKey.getKeyRouting();
    String nextNodeId = keyRouting.getNextNodeId().getName();
    KeyRelayRequest keyRelayRequest =
        KeyRelayRequest.builder()
            .keyId(applicationKey.getId())
            .encriptionKeyId(applicationKey.getEncryptionKeyId())
            .appId(keyRouting.getAppId())
            .encryptedKey(applicationKey.getApplicationKey())
            .build();

    keyRelayClient.keyRelay(keyRelayRequest, nextNodeId);
  }
}
