package cz.cvut.fel.qkd.kms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class KeyManagementSystemForTheSdQkdNetworkApplication {

  public static void main(String[] args) {
    SpringApplication.run(KeyManagementSystemForTheSdQkdNetworkApplication.class, args);
  }
}
