package cz.cvut.fel.qkd.kms.client;

import cz.cvut.fel.qkd.kms.openapi.sdn.ApiClient;
import cz.cvut.fel.qkd.kms.openapi.sdn.api.ControlApi;
import cz.cvut.fel.qkd.kms.openapi.sdn.model.SessionCreationContainer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ControllerClient {

  @Value("${controller.base_path}")
  private String controllerBasepath;

  public void forwardSessionCreationRequest(SessionCreationContainer sessionCreationContainer) {
    ApiClient apiClient = new ApiClient();
    apiClient.setBasePath(controllerBasepath);
    ControlApi controlApi = new ControlApi(apiClient);
  }
}
