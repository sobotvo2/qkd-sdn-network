package cz.cvut.fel.qkd.kms.model.entity;

import jakarta.persistence.*;

import java.time.LocalDateTime;

import lombok.*;

@Entity
@RequiredArgsConstructor
@Data
@Builder
@AllArgsConstructor
public class ApplicationKey {

  @Id private String id;

  //Key material with UTF-8 encoding
  @Column private String applicationKey;

  @Column private boolean decryptedAfterReceiving = false;

  //No reference, because the encryotion keys are mocked
  @Column private String decryptionKeyId;

  @Column private boolean encryptedBeforeSending = false;

  //No reference, because the encryotion keys are mocked
  @Column private String encryptionKeyId;

  @ManyToOne
  @JoinColumn
  private KeyRouting keyRouting;

  @Column private LocalDateTime createdAt;
}
