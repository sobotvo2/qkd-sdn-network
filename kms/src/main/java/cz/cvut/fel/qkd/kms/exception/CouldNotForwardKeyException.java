package cz.cvut.fel.qkd.kms.exception;

public class CouldNotForwardKeyException extends RuntimeException {
  public CouldNotForwardKeyException(String nextNode, String keyId) {
    super("Key with id " + keyId + " could not be forward to node: " + nextNode);
  }
}
