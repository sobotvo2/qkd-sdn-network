package cz.cvut.fel.qkd.kms.service;

import cz.cvut.fel.qkd.kms.model.entity.EncryptionKey;
import cz.cvut.fel.qkd.kms.model.repository.EncryptionKeyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log
@RequiredArgsConstructor
public class MockQKDModule implements  EncryptionKeyService {
  private final EncryptionKeyRepository encryptionKeyRepository;

  @Value("${node.name}")
  private String thisNodeName;

  public EncryptionKey getEncryptionKeyForRelayString(String toNode) {
    return encryptionKeyRepository.getNewEncryptionKeyById(thisNodeName, toNode);
  }

  public String genEncryptionKeyIdById(String id, String fromNode) {
    return encryptionKeyRepository.getNewEncryptionKeyById(id).getEncryptionKey();
  }
}
