package cz.cvut.fel.qkd.kms.model.entity;

import cz.cvut.fel.qkd.kms.openapi.fetch.client.model.Key;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
public class QkdSecuredApplication {
  @Id private String name;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  List<KeyRouting> masterOfApplications;
  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  List<KeyRouting> slaveOfApplications;
}
