package cz.cvut.fel.qkd.kms.rest;

import cz.cvut.fel.qkd.kms.openapi.fetch.server.api.ApiApiDelegate;
import cz.cvut.fel.qkd.kms.openapi.fetch.server.model.*;
import cz.cvut.fel.qkd.kms.service.KeySupplyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log
public class KeyDeliveryApiDelegateImpl implements ApiApiDelegate {
  private static final String SLAVE_SAEID_DEFAULT = "SLAVE";
  private static final String MASTER_SAEID_DEFAULT = "MASTER";
  private final KeySupplyService keySupplyService;

  @Override
  public ResponseEntity<KeyContainer> apiV1KeysSlaveSAEIDEncKeysGet(
      String slaveSAEID, Integer number, Integer size) {
    return ResponseEntity.ok(
        keySupplyService.getKeysToMaster(MASTER_SAEID_DEFAULT, slaveSAEID, number, size));
  }

  @Override
  public ResponseEntity<KeyContainer> apiV1KeysMasterSAEIDDecKeysGet(
      String masterSAEID, String keyID) {
    return ResponseEntity.ok(
        keySupplyService.getKeyToSlave(masterSAEID, SLAVE_SAEID_DEFAULT, keyID));
  }
}
