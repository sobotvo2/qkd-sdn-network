package cz.cvut.fel.qkd.kms.async.publisher;

import cz.cvut.fel.qkd.kms.async.event.EncryptKeyEvent;
import cz.cvut.fel.qkd.kms.async.event.ForwardKeyEvent;
import cz.cvut.fel.qkd.kms.async.event.NewKeyEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Log
public class KeyEventPublisher {
  private final ApplicationEventPublisher eventPublisher;

  public void publishNewKeyEvent(String keyId) {
    NewKeyEvent newKeyEvent = new NewKeyEvent(this, keyId);
    eventPublisher.publishEvent(newKeyEvent);
  }

  public void publishForwardKeyEvent(String keyId) {
    ForwardKeyEvent forwardKeyEvent = new ForwardKeyEvent(this, keyId);
    eventPublisher.publishEvent(forwardKeyEvent);
  }

  public void publishEncryptKeyEvent(String keyId) {
    EncryptKeyEvent encryptKeyEvent = new EncryptKeyEvent(this, keyId);
    eventPublisher.publishEvent(encryptKeyEvent);
  }
}
