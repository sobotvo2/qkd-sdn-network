package cz.cvut.fel.qkd.kms.service;

import cz.cvut.fel.qkd.kms.exception.NoApplicationFoundException;
import cz.cvut.fel.qkd.kms.exception.NoRoutingException;
import cz.cvut.fel.qkd.kms.model.entity.KeyRouting;
import cz.cvut.fel.qkd.kms.model.entity.QkdSecuredApplication;
import cz.cvut.fel.qkd.kms.model.entity.TrustedNode;
import cz.cvut.fel.qkd.kms.model.repository.KeyRoutingRepository;
import cz.cvut.fel.qkd.kms.model.repository.QkdSecuredApplicationRepository;
import cz.cvut.fel.qkd.kms.model.repository.TrustedNodeRepository;
import cz.cvut.fel.qkd.kms.openapi.server.model.SetupApplicationRequest;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log
public class RoutingService {
  private final KeyRoutingRepository keyRoutingRepository;
  private final QkdSecuredApplicationRepository qkdSecuredApplicationRepository;
  private final TrustedNodeRepository trustedNodeRepository;

  public void addNewQkdApplication(SetupApplicationRequest setupApplicationRequest) {
    log.info(setupApplicationRequest.toString() + "was received");
    QkdSecuredApplication masterApplication =
        qkdSecuredApplicationRepository
            .findById(setupApplicationRequest.getQkdlApplication().getServerAppId())
            .orElseThrow(
                () ->
                    new NoApplicationFoundException(
                        setupApplicationRequest.getQkdlApplication().getServerAppId()));

    QkdSecuredApplication slaveApplication =
        qkdSecuredApplicationRepository
            .findById(setupApplicationRequest.getQkdlApplication().getClientAppId().getFirst())
            .orElseThrow(
                () ->
                    new NoApplicationFoundException(
                        setupApplicationRequest.getQkdlApplication().getClientAppId().getFirst()));

    TrustedNode previousNode;
    if (Objects.equals(setupApplicationRequest.getPreviousNodeId(), "")
        || setupApplicationRequest.getPreviousNodeId() == null) {
      previousNode = null;
    } else {
      previousNode =
          trustedNodeRepository
              .findById(setupApplicationRequest.getPreviousNodeId())
              .orElseThrow(
                  () ->
                      new NoRoutingException(
                          setupApplicationRequest.getPreviousNodeId(), "Node does not exist"));
    }
    TrustedNode nextNode;
    if (Objects.equals(setupApplicationRequest.getNextNodeId(), "")
        || setupApplicationRequest.getNextNodeId() == null) {
      nextNode = null;
    } else {
      nextNode =
          trustedNodeRepository
              .findById(setupApplicationRequest.getNextNodeId())
              .orElseThrow(
                  () ->
                      new NoRoutingException(
                          setupApplicationRequest.getNextNodeId(), "Node does not exist"));
    }
    KeyRouting keyRouting =
        keyRoutingRepository.save(
            KeyRouting.builder()
                .appId(setupApplicationRequest.getAppId())
                .nextNodeId(nextNode)
                .previousNodeId(previousNode)
                .masterSae(masterApplication)
                .slaveSae(slaveApplication)
                .appId(setupApplicationRequest.getAppId())
                .build());
    log.info(keyRouting.getAppId() + " was added to routing!");
  }
}
