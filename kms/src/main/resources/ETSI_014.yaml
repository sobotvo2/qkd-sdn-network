openapi: 3.0.0
info:
  title: ETSI GS QKD 014 V1.1.1 REST API
  version: 1.0.0
  description: OpenAPI Specification for ETSI GS QKD 014 V1.1.1 (2019-02)
paths:
  /api/v1/keys/{slave_SAE_ID}/status:
    get:
      summary: Get status
      description: Retrieves status information from a KME to the calling SAE.
      parameters:
        - name: slave_SAE_ID
          in: path
          required: true
          description: URL-encoded SAE ID of the slave SAE.
          schema:
            type: string
      responses:
        '200':
          description: Successful response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Status'
        '400':
          description: Bad request format
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '503':
          description: Error on the server side
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/v1/keys/{slave_SAE_ID}/enc_keys:
    get:
      summary: Get key
      description: Retrieves an encrypted key for a key identified by {slave_SAE_ID}.
      parameters:
        - name: slave_SAE_ID
          in: path
          required: true
          description: URL-encoded SAE ID of the slave SAE.
          schema:
            type: string
        - name: number
          in: query
          required: false
          description: Number of keys requested.
          schema:
            type: integer
        - name: size
          in: query
          required: false
          description: Size of each key in bits.
          schema:
            type: integer
      responses:
        '200':
          description: Successful response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/KeyContainer'
        '400':
          description: Bad request format
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '503':
          description: Error on the server side
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      summary: Get key with options
      description: >-
        Retrieves an encrypted key with additional options for a key identified
        by {slave_SAE_ID}.
      parameters:
        - name: slave_SAE_ID
          in: path
          required: true
          description: URL-encoded SAE ID of the slave SAE.
          schema:
            type: string
      requestBody:
        description: Key request data format
        required: false
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/KeyRequest'
      responses:
        '200':
          description: Successful response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/KeyContainer'
        '400':
          description: Bad request format
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '503':
          description: Error on the server side
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /api/v1/keys/{master_SAE_ID}/dec_keys:
    post:
      summary: Get key container
      description: Retrieves keys matching those previously delivered to a remote master SAE based on the Key IDs supplied from the remote master SAE.
      parameters:
        - name: master_SAE_ID
          in: path
          required: true
          description: URL-encoded SAE ID of master SAE.
          schema:
            type: string
      requestBody:
        description: Key IDs data format
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/KeyIDs'
      responses:
        '200':
          description: Successful response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/KeyContainer'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

    get:
      summary: Get key container for specified simple requests
      description: Retrieves keys matching those previously delivered to a remote master SAE based on the Key IDs supplied from the remote master SAE. Only for specified simple requests.
      parameters:
        - name: master_SAE_ID
          in: path
          required: true
          description: URL-encoded SAE ID of master SAE.
          schema:
            type: string
        - name: key_ID
          in: query
          required: true
          description: Key ID in UUID format.
          schema:
            type: string
      responses:
        '200':
          description: Successful response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/KeyContainer'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
components:
  schemas:
    Status:
      type: object
      properties:
        source_KME_ID:
          type: string
        target_KME_ID:
          type: string
        master_SAE_ID:
          type: string
        slave_SAE_ID:
          type: string
        key_size:
          type: integer
        stored_key_count:
          type: integer
        max_key_count:
          type: integer
        max_key_per_request:
          type: integer
        max_key_size:
          type: integer
        min_key_size:
          type: integer
        max_SAE_ID_count:
          type: integer
        status_extension:
          type: object
    KeyRequest:
      type: object
      properties:
        number:
          type: integer
          description: Number of keys requested. Default value is 1.
        size:
          type: integer
          description: >-
            Size of each key in bits. Default value is defined as key_size in
            Status data format.
        additional_slave_SAE_IDs:
          type: array
          items:
            type: string
          description: >-
            Array of IDs of slave SAEs. Used for specifying two or more slave
            SAEs to share identical keys.
        extension_mandatory:
          type: array
          items:
            type: object
          description: >-
            Array of extension parameters specified as name/value pairs that KME
            shall handle or return an error.
        extension_optional:
          type: array
          items:
            type: object
          description: >-
            Array of extension parameters specified as name/value pairs that KME
            may ignore.
    KeyContainer:
      type: object
      properties:
        keys:
          type: array
          items:
            $ref: '#/components/schemas/Key'

    KeyIDs:
      type: object
      properties:
        keys:
          type: array
          items:
            type: string
    Key:
      type: object
      properties:
        key_ID:
          type: string
        key:
          type: string
    Error:
      type: object
      properties:
        message:
          type: string
          description: Error message
        details:
          type: array
          items:
            type: object
          description: >-
            Array to supply additional detailed error information specified as
            name/value pairs.
