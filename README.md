Software defined quantum key distribution network
-------------------------------------------------
Project is composed from two components
1. [SQ-QKD-CONTROLLER](https://gitlab.fel.cvut.cz/sobotvo2/qkd-sdn-network/-/tree/master/sd_qkd_controller?ref_type=heads) - Controller of the network
2. [KMS](https://gitlab.fel.cvut.cz/sobotvo2/qkd-sdn-network/-/tree/master/kms?ref_type=heads) - Key management system
-------------------------------------------------
How to setup demonstration?
1. Prerequisties: Installed Java 21 JDK
2. Download [folder](https://gitlab.fel.cvut.cz/sobotvo2/qkd-sdn-network/-/tree/master/builds?ref_type=heads)
3. Run each of 4 files (1 Controller and 4 nodes) inside of indviual console (Logs of services are shown in console) with command
command `{path to some java 21 jdk java.exe} -jar {filename}.jar` (Jar files are builded executables of components with added enviroment variables for the KMS, build is described in alternative approach)
4. Succesfull start of service will be shown in the console

Alternatively
1. Build of application using maven requires correctly set [JAVA_HOME](https://www.baeldung.com/java-home-on-windows-mac-os-x-linux) enviroment variable.
1. Individual components can be builded and started in their root folders using maven `\.mvnw.cmd spring-boot:run`
2. KMS need indviual properies to set its as correct node. [File](https://gitlab.fel.cvut.cz/sobotvo2/qkd-sdn-network/-/blob/master/kms/src/main/resources/application.properties?ref_type=heads) must contain correct name and port as is shown in the following table. (Names of nodes are ALPHA, BETA, GAMMA. Ports 8081,8082,8083)
3. Componets will be then builded and started inside Individual consoles.


- | **NODE** | **ADDRESS**| 
- | ALPHA | 8081   |
- | BETA  | 8082   |
- | GAMMA | 8083   |
-------------------------------------------------
Demonstration instruction:
- Adresses of UI of components
- - Controller http://localhost:8080/swagger-ui/index.html#
- - Node ALPHA http://localhost:8081/swagger-ui/index.html#
- - Node BETA  http://localhost:8082/swagger-ui/index.html#
- - Node GAMMA http://localhost:8083/swagger-ui/index.html#

1. All components needs to be setup as shown in previous sections.
2. QKD secured application must be created by [Create application request](http://localhost:8080/swagger-ui/index.html#/Control/createApplicationPost). Start and end node can be any of ALPHA, BETA, GAMMA. Client application must be SLAVE and server must be MASTER, other paramerts are irrational for demonstration. After succesfull execution of the query the 201 resposnse will be returned, or the error with information will be thrown.
3. Key requested by MASTER is need to be done using the [GET KEYS BY MASTER](http://localhost:8081/swagger-ui/index.html#/api/apiV1KeysSlaveSAEIDEncKeysGet) on the Trusted node . SlaveSAEID must be SLAVE, number of keys is variable, size of the key can be set to any number, but will be set 256 bit by the system. Keys with Ids will be returned.
4. Key fetch by SLAVE is need to be done using the [GET KEY BY SLAVE](http://localhost:8083/swagger-ui/index.html#/api/apiV1KeysMasterSAEIDDecKeysGet) on the Trusted node. MasterSAEID must be MASTER, id need to be copied from message in previous point. Requested key will be returned.

Base url of the requests to the nodes can and should be changed to nodes and ports as is shown in the table.
Limitations:
- In UI implemented are only the requests mentioned in demonstration, others are prepared for future development. When they are invoked 501 will be returned.
- SDN will allow only one session between SLAVE and MASTER. When different configuation of network is wanted, all components should be restarted.
-------------------------------------------------------

Lot of code is hidden in generated files, these files will appear when the services are compiled, using `\.mvnw.cmd compile` in root directories.

-------------------------------------------------------
If there is any problem with demonstration, I am happy to help. Contact me on sobotvo2@fel.cvut.cz
