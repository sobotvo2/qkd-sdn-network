package cz.cvut.fel.qkd.sd_qkd_controller.exception;

public class NoSuchNodeExistsException extends RuntimeException {

  public NoSuchNodeExistsException(String nodeName) {
    super(nodeName + " does not exist");
  }
}
