package cz.cvut.fel.qkd.sd_qkd_controller.model.repository;

import cz.cvut.fel.qkd.sd_qkd_controller.model.entity.QkdSecuredApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QkdSecuredApplicationRepository
    extends JpaRepository<QkdSecuredApplication, String> {}
