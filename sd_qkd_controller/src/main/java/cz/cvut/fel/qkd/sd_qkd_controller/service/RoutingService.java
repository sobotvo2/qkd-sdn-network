package cz.cvut.fel.qkd.sd_qkd_controller.service;

import cz.cvut.fel.qkd.sd_qkd_controller.client.RoutingClient;
import cz.cvut.fel.qkd.sd_qkd_controller.exception.*;
import cz.cvut.fel.qkd.sd_qkd_controller.model.entity.QkdSecuredApplication;
import cz.cvut.fel.qkd.sd_qkd_controller.model.entity.QkdqiApplication;
import cz.cvut.fel.qkd.sd_qkd_controller.model.entity.TrustedNode;
import cz.cvut.fel.qkd.sd_qkd_controller.model.mapper.RoutingMapper;
import cz.cvut.fel.qkd.sd_qkd_controller.model.repository.QkdSecuredApplicationRepository;
import cz.cvut.fel.qkd.sd_qkd_controller.model.repository.QkdqiApplicationRepository;
import cz.cvut.fel.qkd.sd_qkd_controller.model.repository.TrustedNodeRepository;
import cz.cvut.fel.qkd.sd_qkd_controller.openapi.node.model.SessionConfigurationContainer;
import cz.cvut.fel.qkd.sd_qkd_controller.openapi.node.model.SetupApplicationRequestQkdlApplication;
import cz.cvut.fel.qkd.sd_qkd_controller.openapi.server.model.CreateApplicationPost201Response;
import cz.cvut.fel.qkd.sd_qkd_controller.openapi.server.model.CreateApplicationPostRequest;
import java.time.OffsetDateTime;
import java.util.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

@Service
@Log
@RequiredArgsConstructor
public class RoutingService {
  private final QkdqiApplicationRepository qkdqiApplicationRepository;
  private final RoutingClient routingClient;
  private final RoutingMapper routingMapper;
  private final TrustedNodeRepository trustedNodeRepository;
  private final QkdSecuredApplicationRepository qkdSecuredApplicationRepository;

  public CreateApplicationPost201Response createApplicationSession(
      CreateApplicationPostRequest createApplicationPostRequest) {
    log.info("Create Application Session " + createApplicationPostRequest);

    // CHECk EXISTENCE OF NODES
    TrustedNode startNode = getTrustedNodeByName(createApplicationPostRequest.getStartNodeId());
    TrustedNode endNode = getTrustedNodeByName(createApplicationPostRequest.getEndNodeId());
    // CHECK EXISTENCE OF SECURED APPLICATIONS
    QkdSecuredApplication masterApp =
        getQksSecuredApplicationByName(createApplicationPostRequest.getServerAppId());
    QkdSecuredApplication slaveApp =
        getQksSecuredApplicationByName(createApplicationPostRequest.getClientAppId());
    long countOfAlreadyExistingApplications =
        qkdqiApplicationRepository.findAll().stream()
            .filter(
                qkdqiApplication ->
                    Objects.equals(
                            qkdqiApplication.getMasterApp().getName(),
                            createApplicationPostRequest.getServerAppId())
                        && Objects.equals(
                            qkdqiApplication.getSlaveApp().getName(),
                            createApplicationPostRequest.getClientAppId()))
            .count();

    if (countOfAlreadyExistingApplications > 0) {
      throw new RoutingAlreadyExistsException(
          createApplicationPostRequest.getServerAppId(),
          createApplicationPostRequest.getClientAppId());
    }
    if(Objects.equals(startNode.getName(), endNode.getName())){
      throw new RoutingErrorException(startNode.getName(), endNode.getName());
    }

    QkdqiApplication qkdqiApplication =
        QkdqiApplication.builder()
            .serverNode(startNode)
            .clientNode(endNode)
            .masterApp(masterApp)
            .slaveApp(slaveApp)
            .build();
    List<String> path =
        createRoutingPath(
            createApplicationPostRequest.getStartNodeId(),
            createApplicationPostRequest.getEndNodeId());
    qkdqiApplication = qkdqiApplicationRepository.save(qkdqiApplication);
    try {
      notifyAboutRouting(
          path,
          qkdqiApplication.getId(),
          createApplicationPostRequest.getStartNodeId(),
          createApplicationPostRequest.getEndNodeId(),
          createApplicationPostRequest.getServerAppId(),
          createApplicationPostRequest.getClientAppId());
    } catch (CouldNotSendRoutingException couldNotSendRoutingException) {
      //Delete session if was not created
      qkdqiApplicationRepository.deleteById((long) qkdqiApplication.getId());
      throw new CouldNotSendRoutingException(couldNotSendRoutingException.getNodeName());
    }
    return CreateApplicationPost201Response.builder()
        .appId(String.valueOf(qkdqiApplication.getId()))
        .localQkdnId(qkdqiApplication.getServerNode().getName())
        .remoteQkdnId(qkdqiApplication.getClientNode().getName())
        .clientAppId(List.of(createApplicationPostRequest.getClientAppId()))
        .serverAppId(createApplicationPostRequest.getServerAppId())
        .creationTime(OffsetDateTime.now())
        .expirationTime(OffsetDateTime.now().plusHours(12))
        .appStatus("OPERATIONAL")
        .build();
  }

  private void notifyAboutRouting(
      List<String> path,
      int id,
      String clientNode,
      String serverNode,
      String serverNodeId,
      String clientNodeId) {
    for (String node : path) {
      SessionConfigurationContainer sessionConfigurationContainer =
          new SessionConfigurationContainer();
      if (Objects.equals(node, clientNode)) {
        sessionConfigurationContainer.setPreviousNodeId("");
        sessionConfigurationContainer.setNextNodeId(path.get(1));
      } else if (Objects.equals(node, serverNode)) {
        sessionConfigurationContainer.setPreviousNodeId(path.get(path.size() - 2));
        sessionConfigurationContainer.setNextNodeId("");
      } else {
        int position = path.indexOf(node);
        sessionConfigurationContainer.setPreviousNodeId(path.get(position - 1));
        sessionConfigurationContainer.setNextNodeId(path.get(position + 1));
      }
      sessionConfigurationContainer.appId(String.valueOf(id));
      sessionConfigurationContainer.setQkdlApplication(
          SetupApplicationRequestQkdlApplication.builder()
              .appId(String.valueOf(id))
              .clientAppId(List.of(clientNodeId))
              .serverAppId(serverNodeId)
              .localQkdnId(node)
              .remoteQkdnId("")
              .build());
      sendToNodeRoutingInfo(node, sessionConfigurationContainer);
    }
  }

  private void sendToNodeRoutingInfo(
      String node, SessionConfigurationContainer sessionConfigurationContainer) {
    String address = getTrustedNodeByName(node).getAddress();
    log.info(
        "Sending for node "
            + node
            + " to address "
            + address
            + " routing "
            + sessionConfigurationContainer.toString());
    boolean ret =
        routingClient.sendRoutingMessage(
            address,
            routingMapper.mapSessionConfigurationContainerToSetupApplicationRequest(
                sessionConfigurationContainer));
    if (!ret) {
      throw new CouldNotSendRoutingException(node);
    }
  }

  private List<String> createRoutingPath(String startNodeName, String endNodeName) {
    log.info("Create Routing Path " + startNodeName + " " + endNodeName);
    // Initialization
    Queue<List<String>> queue = new LinkedList<>();
    List<String> solution = new ArrayList<>();
    List<String> visited = new ArrayList<>();
    visited.add(startNodeName);
    // Initialize queue with paths from the start node
    for (String node : getConnectedNodesNames(startNodeName)) {
      List<String> arr = new ArrayList<>();
      arr.add(startNodeName);
      arr.add(node);
      queue.add(arr);
    }
    while (!queue.isEmpty()) {
      List<String> nodes = queue.poll();
      // Skip if the last node in the path was already visited
      if (visited.contains(nodes.getLast())) {
        continue;
      } else {
        visited.add(nodes.getLast());
      }
      // End if solution was found
      if (nodes.contains(endNodeName)) {
        solution = nodes;
        break;
      } else {
        String lastNode = nodes.getLast();
        // Insert new paths from node
        getConnectedNodesNames(lastNode)
            .forEach(
                x -> {
                  ArrayList<String> addition = new ArrayList<>(nodes);
                  addition.add(x);
                  queue.add(addition);
                });
      }
    }
    // If solution was not found throw an error
    if (solution.isEmpty()) {
      throw new RoutingErrorException(startNodeName, endNodeName);
    }
    log.info("Solution route is " + solution.toString());
    // Return solution
    return solution;
  }

  private TrustedNode getTrustedNodeByName(String name) {
    return trustedNodeRepository
        .findById(name)
        .orElseThrow(() -> new NoSuchNodeExistsException(name));
  }

  private List<String> getConnectedNodesNames(String nodeName) {
    return getTrustedNodeByName(nodeName).getConnectedNodes().stream()
        .map(TrustedNode::getName)
        .toList();
  }

  private QkdSecuredApplication getQksSecuredApplicationByName(String name) {
    return qkdSecuredApplicationRepository
        .findById(name)
        .orElseThrow(() -> new NoSuchQkdSecuredApplicationException(name));
  }
}
