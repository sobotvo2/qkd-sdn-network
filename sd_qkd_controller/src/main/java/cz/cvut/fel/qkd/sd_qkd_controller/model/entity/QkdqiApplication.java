package cz.cvut.fel.qkd.sd_qkd_controller.model.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
public class QkdqiApplication {
  @GeneratedValue @Id private int id;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
  @JoinColumn()
  private TrustedNode clientNode;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
  @JoinColumn()
  private TrustedNode serverNode;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
  @JoinColumn()
   private QkdSecuredApplication masterApp;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
  @JoinColumn()
  private QkdSecuredApplication slaveApp;
}
