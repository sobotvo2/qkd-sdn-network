package cz.cvut.fel.qkd.sd_qkd_controller.client;

import cz.cvut.fel.qkd.sd_qkd_controller.openapi.node.ApiClient;
import cz.cvut.fel.qkd.sd_qkd_controller.openapi.node.api.ApplicationSetupApi;
import cz.cvut.fel.qkd.sd_qkd_controller.openapi.node.model.SetupApplicationRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

@RequiredArgsConstructor
@Component
@Log
public class RoutingClient {
  public boolean sendRoutingMessage(
      String targetAddress, SetupApplicationRequest setupApplicationRequest) {
    ApiClient apiClient = new ApiClient();
    apiClient.setBasePath(targetAddress);
    ApplicationSetupApi applicationSetupApi = new ApplicationSetupApi(apiClient);
    log.info("Sending send routing request to " + targetAddress);
    try {
      applicationSetupApi.setupApplication(setupApplicationRequest);
      log.info("Routing request sent to " + targetAddress + " successful  ly");
      return true;
    } catch (RestClientException e) {
      log.info(e.getMessage());
      log.info(
          RoutingClient.class.getSimpleName()
              + " failed to send routing request to "
              + targetAddress);
      return false;
    }
  }
}
