package cz.cvut.fel.qkd.sd_qkd_controller.model.entity;

import jakarta.persistence.*;
import java.util.List;
import lombok.*;

@Data
@RequiredArgsConstructor
@Entity
@Builder
@AllArgsConstructor
public class TrustedNode {
  @Id
  private  String name;
  private  String address;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
          name = "connected_node",
          joinColumns = @JoinColumn(name = "start_name"),
          inverseJoinColumns = @JoinColumn(name = "end_name")
  )
  private List<TrustedNode> connectedNodes;

  @ManyToMany(mappedBy = "connectedNodes")
  private List<TrustedNode> isConnectedToNodes;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  List<QkdqiApplication> startNodeOfApplications;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  List<QkdqiApplication> endNodeOfApplications;
}
