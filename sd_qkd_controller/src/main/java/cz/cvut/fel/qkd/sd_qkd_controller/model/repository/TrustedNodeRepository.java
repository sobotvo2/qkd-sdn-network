package cz.cvut.fel.qkd.sd_qkd_controller.model.repository;

import cz.cvut.fel.qkd.sd_qkd_controller.model.entity.TrustedNode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrustedNodeRepository extends JpaRepository<TrustedNode, String> {}
