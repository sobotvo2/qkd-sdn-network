package cz.cvut.fel.qkd.sd_qkd_controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdQkdControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdQkdControllerApplication.class, args);
	}

}
