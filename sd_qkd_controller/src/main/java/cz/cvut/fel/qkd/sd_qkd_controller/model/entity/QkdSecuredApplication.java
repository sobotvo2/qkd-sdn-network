package cz.cvut.fel.qkd.sd_qkd_controller.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
public class QkdSecuredApplication {
  @Id private String name;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  List<QkdqiApplication> masterOfApplications;
  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  List<QkdqiApplication> slaveOfApplications;
}
