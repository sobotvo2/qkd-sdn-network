package cz.cvut.fel.qkd.sd_qkd_controller.rest;

import cz.cvut.fel.qkd.sd_qkd_controller.openapi.server.api.CreateApplicationApiDelegate;
import cz.cvut.fel.qkd.sd_qkd_controller.openapi.server.model.CreateApplicationPost201Response;
import cz.cvut.fel.qkd.sd_qkd_controller.openapi.server.model.CreateApplicationPostRequest;
import cz.cvut.fel.qkd.sd_qkd_controller.service.RoutingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreateApplicationApiDelegateImpl implements CreateApplicationApiDelegate {
  private final RoutingService routingService;

  @Override
  public ResponseEntity<CreateApplicationPost201Response> createApplicationPost(
      CreateApplicationPostRequest createApplicationPostRequest) {
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(routingService.createApplicationSession(createApplicationPostRequest));
  }
}
