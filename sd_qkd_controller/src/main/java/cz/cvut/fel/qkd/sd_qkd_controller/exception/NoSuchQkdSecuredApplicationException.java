package cz.cvut.fel.qkd.sd_qkd_controller.exception;

public class NoSuchQkdSecuredApplicationException extends RuntimeException {
  public NoSuchQkdSecuredApplicationException(String qkdSecuredApplicationId) {
    super(qkdSecuredApplicationId + " does not exist");
  }
}
