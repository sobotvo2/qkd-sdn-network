package cz.cvut.fel.qkd.sd_qkd_controller.exception;

public class RoutingAlreadyExistsException extends RuntimeException {
  public RoutingAlreadyExistsException(String serverApp, String clientApp) {
    super(
        "Session between server and app "
            + serverApp
            + " and client: "
            + clientApp
            + " already exists, so new cannot be created!");
  }
}
