package cz.cvut.fel.qkd.sd_qkd_controller.exception;

import static org.springframework.http.ResponseEntity.status;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandler {

  @ResponseBody
  @ExceptionHandler(NoSuchNodeExistsException.class)
  ResponseEntity<ExceptionDto> handleNoSuchNodeExistsException(
      final NoSuchNodeExistsException exception) {
    return status(HttpStatus.NOT_FOUND)
        .body(
            new ExceptionDto(
                NoSuchNodeExistsException.class.getTypeName(), exception.getMessage()));
  }

  @ResponseBody
  @ExceptionHandler(CouldNotSendRoutingException.class)
  ResponseEntity<ExceptionDto> handleCouldNotSendRoutingException(
      final CouldNotSendRoutingException exception) {
    return status(HttpStatus.NOT_FOUND)
        .body(
            new ExceptionDto(
                CouldNotSendRoutingException.class.getTypeName(), exception.getMessage()));
  }

  @ResponseBody
  @ExceptionHandler(RoutingErrorException.class)
  ResponseEntity<ExceptionDto> handleRoutingErrorException(final RoutingErrorException exception) {
    return status(HttpStatus.CONFLICT)
        .body(new ExceptionDto(RoutingErrorException.class.getTypeName(), exception.getMessage()));
  }

  @ResponseBody
  @ExceptionHandler(NoSuchQkdSecuredApplicationException.class)
  ResponseEntity<ExceptionDto> handleNoSuchQkdSecuredApplicationException(
      final NoSuchQkdSecuredApplicationException exception) {
    return status(HttpStatus.NOT_FOUND)
        .body(
            new ExceptionDto(
                NoSuchQkdSecuredApplicationException.class.getTypeName(), exception.getMessage()));
  }

  @ResponseBody
  @ExceptionHandler(RoutingAlreadyExistsException.class)
  ResponseEntity<ExceptionDto> handleNoRoutingAlreadyExistsException(
      final RoutingAlreadyExistsException exception) {
    return status(HttpStatus.CONFLICT)
        .body(
            new ExceptionDto(
                RoutingAlreadyExistsException.class.getTypeName(), exception.getMessage()));
  }
}
