package cz.cvut.fel.qkd.sd_qkd_controller.exception;

public class RoutingErrorException extends RuntimeException {
  public RoutingErrorException(String startNode, String endNode) {
    super("No routing exists between " + startNode + " and " + endNode);
  }
}
