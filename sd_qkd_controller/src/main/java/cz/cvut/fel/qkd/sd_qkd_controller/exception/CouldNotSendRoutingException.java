package cz.cvut.fel.qkd.sd_qkd_controller.exception;

import lombok.Getter;

@Getter
public class CouldNotSendRoutingException extends RuntimeException {
  private final String nodeName;

  public CouldNotSendRoutingException(String nodeName) {
    super("Could not send routing information for " + nodeName);
    this.nodeName = nodeName;
  }
}
