package cz.cvut.fel.qkd.sd_qkd_controller.model.repository;

import cz.cvut.fel.qkd.sd_qkd_controller.model.entity.QkdqiApplication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QkdqiApplicationRepository extends JpaRepository<QkdqiApplication, Long> {}
