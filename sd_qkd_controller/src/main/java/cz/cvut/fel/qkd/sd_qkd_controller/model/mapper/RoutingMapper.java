package cz.cvut.fel.qkd.sd_qkd_controller.model.mapper;

import cz.cvut.fel.qkd.sd_qkd_controller.openapi.node.model.SessionConfigurationContainer;
import cz.cvut.fel.qkd.sd_qkd_controller.openapi.node.model.SetupApplicationRequest;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
public interface RoutingMapper {
  SetupApplicationRequest mapSessionConfigurationContainerToSetupApplicationRequest(
      SessionConfigurationContainer sessionConfigurationContainer);
}
